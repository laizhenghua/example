package com.laizhenghua.example.utils;

import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 */
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;
    public R() {
        put("code", 200);
        put("msg", "success");
    }
    public R setData(Object data){
        put("data",data);
        return this;
    }
    /**
     * @description: 自定义的泛型方法，利用fastJson进行逆转，来适应我们的业务
     * @author: laizhenghua
     * @date: 2021/1/23 19:22
     * @param: toParseType
     * @return: T
     */
    public <T> T getData(TypeReference<T> toParseType){
        Object data = get("data"); // 默认是map
        String str = JSON.toJSONString(data);
        T t = JSON.parseObject(str, toParseType);
        return t;
    }
    public <T> T getData(String key,TypeReference<T> toParseType){
        String str = JSON.toJSONString(get(key));
        T t = JSON.parseObject(str,toParseType);
        return t;
    }

    public static R error(int code, String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok() {
        return new R();
    }

    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public  Integer getCode() {
        return (Integer) this.get("code");
    }
}
