package com.laizhenghua.example.dao;

import com.laizhenghua.example.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @description:
 * @author: laizhenghua
 * @date: 2022/4/30 18:32
 */
public interface UserDao extends JpaRepository<UserEntity, Integer> {
}
