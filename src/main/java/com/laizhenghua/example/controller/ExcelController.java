package com.laizhenghua.example.controller;

import com.laizhenghua.example.configuration.QueryParamDTO;
import com.laizhenghua.example.service.ExcelService;
import com.laizhenghua.example.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * @description:
 * @date: 2022/4/26 13:28
 */
@RestController
@RequestMapping(value = "excel")
public class ExcelController {
    @Autowired
    private ExcelService excelService;

    /**
     * 获取xml配置文件信息
     * @param queryParam
     * @return
     */
    @RequestMapping(value = "/getConfigInfo", method = RequestMethod.POST)
    public R getConfigInfo(@RequestBody QueryParamDTO queryParam) {
        return R.ok().setData(excelService.getConfigInfo(queryParam));
    }

    /**
     * Excel通用导入数据接口
     * @param file
     * @param queryParam
     * @return
     */
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public R importData(@RequestParam(value = "file") MultipartFile file, @RequestBody QueryParamDTO queryParam) {
        Map<String, String> result = excelService.importData(file, queryParam);
        String error = result.get("error");
        if (error != null) {
            return R.error(500, error);
        }
        return R.ok().put("data", result);
    }
}
