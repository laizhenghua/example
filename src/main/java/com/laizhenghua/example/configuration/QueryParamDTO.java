package com.laizhenghua.example.configuration;

/**
 * @description:
 * @date: 2022/4/26 13:37
 */
public class QueryParamDTO {
    private String filePath;
    private String fileName;
    private String configName;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }
}
