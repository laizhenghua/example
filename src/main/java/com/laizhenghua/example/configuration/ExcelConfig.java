package com.laizhenghua.example.configuration;

import java.util.List;

/**
 * @author laizhenghua
 * @description: excel配置
 * @date: 2022/4/25 22:05
 */
public class ExcelConfig {
    private String filePath;
    private String name; // configName
    private String dbName;
    private String packagePath; // 类的全路径名称
    private List<ColumnConfig> columnConfigList;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public void setPackagePath(String packagePath) {
        this.packagePath = packagePath;
    }

    public List<ColumnConfig> getColumnConfigList() {
        return columnConfigList;
    }

    public void setColumnConfigList(List<ColumnConfig> columnConfigList) {
        this.columnConfigList = columnConfigList;
    }
}
