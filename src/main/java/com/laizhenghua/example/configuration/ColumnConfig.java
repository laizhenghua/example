package com.laizhenghua.example.configuration;

/**
 * @author laizhenghua
 * @description: Java实体与表字段的配置
 * @date: 2022/4/25 22:14
 */
public class ColumnConfig {
    private String name; // 实体属性名
    private String type; // 实体属性数据类型
    private String columnName; // 表字段名
    private Boolean notNull = false; // 是否可以为空 默认为 false
    private String title; // Excel表头字段名

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Boolean getNotNull() {
        return notNull;
    }

    public void setNotNull(Boolean notNull) {
        this.notNull = notNull;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
