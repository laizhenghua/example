package com.laizhenghua.example.service;

import com.laizhenghua.example.configuration.ExcelConfig;
import com.laizhenghua.example.configuration.QueryParamDTO;
import org.apache.poi.ss.usermodel.Cell;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @date: 2022/4/26 13:29
 */
public interface ExcelService {
    ExcelConfig getConfigInfo(QueryParamDTO queryParam);

    Map<String, String> importData(MultipartFile file, QueryParamDTO queryParam);

    List<Map<String, Cell>> resolveExcelData(MultipartFile file);

    <E> List<E> convertEntity(ExcelConfig config, List<Map<String, Cell>> data, Class<E> clazz);

    <E> void writeOutputStreamByConfig(ExcelConfig config, List<E> list, FileOutputStream fos);

    void write(InputStream inputStream, OutputStream outputStream);
}
