package com.laizhenghua.example.service;

import com.laizhenghua.example.configuration.QueryParamDTO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @description:
 * @author: laizhenghua
 * @date: 2022/5/1 18:04
 */
public interface UserService {
    Map<String, String> importExcelData(MultipartFile file, QueryParamDTO queryParam);

    String exportData(QueryParamDTO queryParam, HttpServletResponse response);
}
